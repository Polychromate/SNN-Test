import numpy as np

def specifyLayers(layers):
    for i in range(0, layers):
        globals()[f"l{i}"] = []
        
class neuron:
    def __init__(self, layer, weights, type):
        
        self.layer = layer
        self.weights = weights
        self.type = type
        self.value = 0
        self.bias = 0
        
    def activation(self):
        
        calcTotal = 0
        
        if self.type == 'input':
            return "Requested neuron is an input neuron, it cannot be processed."
        else:
            prevLayer = self.layer - 1
            
            for i in globals()['l' + str(prevLayer)]:
                
                calcTotal += self.weights[0] * i.value
            
            self.value = np.tanh(calcTotal) + self.bias
            
            return self.value
    
def createNetwork(inputNeurons, hiddenNeurons, outputNeurons, layerCount):
    
    specifyLayers(layerCount) # Calls the specify layers function to create a list variable for each layer.
    weightCount = [] # Declares a list for the amount of weights a neuron will have.
    
    for i in range(inputNeurons): # Creates all the input neurons by appending them to the input layer list.
        l0.append(neuron(0, [], 'input'))
        
    for i in range(layerCount - 2): # 
        weightCount = []
        
        for a in range(len(globals()['l' + str(i)])):
            weightCount.append(0)
            
        for e in range(hiddenNeurons):
            globals()['l' + str(i + 1)].append(neuron(i + 1, weightCount, 'Hidden'))
            
    weightCount = [] # Resets the list of weights.
    
    for a in range(len(globals()['l' + str(layerCount - 2)])):
        weightCount.append(0)
        
    for i in range(outputNeurons):
        globals()['l' + str(layerCount - 1)].append(neuron(layerCount, weightCount, 'output'))


createNetwork(7,10,4,7)

# * Test Code
specifyLayers(3)
l0.append(neuron(0, [], 'input'))
l0[0].value = 0.55
i = neuron(1, [0.5825324], 'hidden')   
print(i.activation())