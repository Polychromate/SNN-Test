import numpy as np
import tkinter as tk
import tkinter.scrolledtext as st
import matplotlib.pyplot as plot
import datetime
import neuronProcessing as nl
from matplotlib.figure import Figure
from math import fabs
from random import randint
from tkinter import ttk
from matplotlib.backends.backend_tkagg import (
    FigureCanvasTkAgg,
    NavigationToolbar2Tk
)

root = tk.Tk()

# * Misc. Variables

elapsedTime = 0
isTraining = False

# * Creates the user interface 

window_width = 1100
window_height = 700

screen_width = root.winfo_screenwidth()
screen_height = root.winfo_screenheight()

root.title("'Strategic' Neural Network Evalulation UI")

center_x = int(screen_width/2 - window_width / 2)
center_y = int(screen_height/2 - window_height / 2)

root.geometry(f'{window_width}x{window_height}+{center_x}+{center_y}')

root.resizable(False, False)

# * Initializes the widets

graphFrame = tk.Frame(root, width=window_width*0.6, height=window_width*0.6, bg = '#cccccc')
graphFrame.pack(side='left', padx=(20,0), pady=(20,20))

def disable_button():
    root.quit()
    root.destroy()

tk.Button(root, text="Quit", command=disable_button, fg="white",bg="black", width=int(window_width*0.4), height=1).pack(padx=20, pady=20, side='top')

settingsFrame = tk.Frame(root, width=window_width*0.4, height=window_height*0.4, bg = '#cccccc')
settingsFrame.pack(side='top', padx=(20,20), pady=(0,0))
innerFrame = tk.Frame(settingsFrame, width=window_width*0.4, height=window_height*0.4)
innerFrame.pack(side='bottom', padx=(10,10), pady=(0,10))

consoleFrame = tk.Frame(root, width=window_width*0.4, height=window_height*0.6, bg = '#cccccc')
consoleFrame.pack(side='bottom', padx=(20,20), pady=(20,20))

# * Creates the Console

consoleWidget = st.ScrolledText(consoleFrame, width=int(window_width*0.4), height=int(window_height*0.6))
consoleWidget.pack(padx=10, pady=10)

consoleWidget.tag_config('info', foreground='black') # Adds color coding to console logs
consoleWidget.tag_config('issue', foreground='yellow')
consoleWidget.tag_config('error', foreground='red')

consoleWidget.insert(tk.INSERT, "C: Welcome to the console, \n", 'info') # logs basic info to the console
consoleWidget.insert(tk.INSERT, "C: Errors/Info will show up here", 'error')

consoleWidget.configure(state='disabled') # Disables text input in the console

# * Settings frame code

settingFrameTitle = tk.Label(settingsFrame, height=0, width=200, text='Settings Panel', bg = '#cccccc', anchor='w')
settingFrameTitle.pack(side='top', padx=(10, 10), pady=(5, 5))

currentGenerationLabel = tk.Label(innerFrame, text='Current Generation: 0', anchor='w', width=48)
currentGenerationLabel.pack(side='top', padx=(0, 10), pady=(5, 0))

mutationChanceLabel = tk.Label(innerFrame, text='Current Mutation Chance: 0', anchor='w', width=48)
mutationChanceLabel.pack(side='top', padx=(0, 10), pady=(5, 0))

currentLossLabel = tk.Label(innerFrame, text='Current Loss: 0', anchor='w', width=48)
currentLossLabel.pack(side='top', padx=(0, 10), pady=(5, 5))

seperator1 = ttk.Separator(innerFrame, orient='horizontal')
seperator1.pack(fill='x')

currentGraphLabel = tk.Label(innerFrame, text='Current Graph:', anchor='w', width=48)
currentGraphLabel.pack(side='top', padx=(0, 10), pady=(5, 5))

changeGraph = ttk.Combobox(innerFrame, values=['Dummy Numbers', 'Loss per instance', 'Avg. Loss per generation', 'Max. Loss per generation'], width=54, justify=tk.LEFT)
changeGraph.pack(side='top', padx=(0, 0), pady=(0, 10))

seperator2 = ttk.Separator(innerFrame, orient='horizontal')
seperator2.pack(fill='x')

elapsedTimeLabel = tk.Label(innerFrame, text='Elapsed Time: 0:00:00', height=0, width=23)
elapsedTimeLabel.pack(side='right', padx=(0, 10), pady=(5, 5))

startTrainingButton = tk.Button(innerFrame, width=0, height=0, text="Start Training")
stopTrainingButton = tk.Button(innerFrame, width=0, height=0, text="Stop Training")

startTrainingButton.pack(side='left', padx=(10,10), pady=10)
stopTrainingButton.pack(side='left', padx=(0,10), pady=10)

# * Graph code

font = {'size'   : 40}
plot.rc('font', **font)

fig = Figure(figsize=(32, 31), dpi=20)

plot.figure(facecolor='yellow')
canvas = FigureCanvasTkAgg(fig, master=graphFrame)  # A tk.DrawingArea.

canvas.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH, expand=1)

toolbar = NavigationToolbar2Tk(canvas, graphFrame)
toolbar.update()
canvas.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH, expand=1)

def plotgraph(x, y):
    fig.clear()
    fig.add_subplot(111).plot(x, y, linewidth=9.0)
    canvas.draw_idle()
    
axis = fig.add_subplot(111)

testx = np.ndarray.tolist(np.arange(1,41))
testy = np.ndarray.tolist(np.random.randint(40, size=40))

def updateCurrentGraph(gx, gy, ylabel, xlabel, title):
    plotgraph(gx, gy)
    
    axis.set_ylabel(ylabel)
    axis.set_xlabel(xlabel)
    axis.set_title(title)
    
    testx.pop(0)
    testx.append(int(testx[-1] + 1))
    
    testy.pop(0)
    testy.append(int(randint(1, 40)))
    
    root.after(500, updateCurrentGraph, gx, gy, ylabel, xlabel, title)

updateCurrentGraph(testx, testy, "Loss", "Time", "Loss over time")

root.mainloop()
